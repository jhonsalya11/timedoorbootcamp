<?php

    
    $serverName = "localhost";
    $username = "root";
    $password = "";
    $databaseName = "challenge30";

    // Create connection
    $conn = mysqli_connect($serverName, $username, $password, $databaseName);

    // // Check connection
    // if ($conn->connect_error) {
    //     die("Connection failed: " . $conn->connect_error);
    // }
    // echo "Connected successfully";

    function query($query)
    {
        global $conn;
        $result = mysqli_query($conn, $query);
        $rows = [];
        while($row = mysqli_fetch_assoc($result)){
            $rows[] = $row;
        }
        //bentuknya array associative
        return $rows;
    }

    function add($query)
    {
        global $conn;

        mysqli_query($conn, $query);
    }

?>