<?php 
class App
{
    protected $controller = 'Post';
    protected $method = 'index';
    protected $params = [""];

    public function __construct(){
        require_once 'controllers/' . $this->controller . '.php';
        $this->controller = new $this->controller;

        if (isset($_POST["submit"])) {
            $valid = $this->validation();
            if (empty($valid)){
                $this->method = 'add';
            } else {
                // array_push($params,"Fill the forms");
                $this->params = [$valid];
            }
        }

        //jalankan controller & method, serta kirimkan params jika ada
        call_user_func_array([$this->controller, $this->method], $this->params);
        // echo 'OK!';
    }

    public function validation(){
        $msg = '';
        if (empty($_POST["title"])) {
            $msg = $msg . 'Title must be filled in<br>';
        } else if (strlen($_POST["title"]) < 10 || strlen($_POST["title"]) > 32 ) {
            $msg = $msg . 'Your title must be 10 to 32 characters long<br>';
        }

        if (empty($_POST["body"])) {
            $msg = $msg . 'Body must be filled in<br>';
        } else if (strlen($_POST["body"]) < 10 || strlen($_POST["body"]) > 200 ) {
            $msg = $msg . 'Your message must be 10 to 200 characters long<br>';
        }
        
        return $msg;
    }
}

?>