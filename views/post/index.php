<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="resource/style.css">
</head>
<body>
    <div class="container">
        <div class="form-container">
            <div class="message">
                <p><?php echo $msg ?></p>
            </div>
            <form action="http://localhost/challenge30/" method="post">
                <div>
                    <label for="title">Title : </label>
                    <br>
                    <input type="text" id="title" name="title">
                </div>
                <div>
                    <label for="body">Body : </label>
                    <br>
                    <textarea name="body" rows="5" cols="40"></textarea>
                </div>
                <div>
                    <button type="submit" name="submit" class="button">Submit</button>
                </div>
            </form>
        </div>

        <div class="posts">
            <?php foreach($posts as $post) : ?>
                <hr>
                <h3><?php echo $post["title"] ?></h3>
                <p><?php echo $post["body"] ?></p>
                <p class="post-created"><?php echo date( "m-d-Y H:i", strtotime($post["created_at"])) ?></p>
            <?php endforeach; ?>
            <hr>
        </div>
    </div>
</body>
</html>