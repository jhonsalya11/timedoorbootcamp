<?php 
class Post extends Controller{
    public function index($msg)
    {
        $posts = $this->model()->getAllPost();
        $this->view($posts, $msg);
        echo "Post Page";
    }

    public function add(){
        $this->model()->addNewPost($_POST);

        // redirect browser
        header('Location: http://localhost/challenge30/');
    }
}

?>